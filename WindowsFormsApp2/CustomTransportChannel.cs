﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opc.Ua.Bindings.Custom
{
    public class CustomTransportChannelFactory : ITransportChannelFactory
    {
        public ITransportChannel Create()
        {
            return new UaSCUaBinaryTransportChannel(new CustomMessageSocketFactory());
        }
    }
    public class CustomMessageSocketFactory : IMessageSocketFactory
    {
        /// <summary>
        /// The method creates a new instance of a custom message socket
        /// </summary>
        /// <returns> the message socket</returns>
        public IMessageSocket Create(
                IMessageSink sink,
                BufferManager bufferManager,
                int receiveBufferSize
            )
        {
            // plug in your custom socket implementation here
            return new CustomMessageSocket(sink, bufferManager, receiveBufferSize);
        }

        /// <summary>
        /// Gets the implementation description.
        /// </summary>
        /// <value>The implementation string.</value>
        public string Implementation { get { return "UA-Custom"; } }

    }
    public class CustomMessageSocket : TcpMessageSocket // IMessageSocket
    {
        #region Constructors
        /// <summary>
        /// Creates an unconnected socket.
        /// </summary>
        public CustomMessageSocket(
            IMessageSink sink,
            BufferManager bufferManager,
            int receiveBufferSize) :
            base(sink, bufferManager, receiveBufferSize)
        { }
        #endregion
    }
}
