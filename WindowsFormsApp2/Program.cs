﻿using Opc.Ua;
using Opc.Ua.Bindings.Custom;
using Opc.Ua.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ApplicationInstance application = new ApplicationInstance();
            application.ApplicationName = "UA Sample Client";

            application.ApplicationType = ApplicationType.Client;
            application.ConfigSectionName = "Opc.Ua.SampleClient";

            WcfChannelBase.g_CustomTransportChannel = new CustomTransportChannelFactory();

            try
            {
                string filePath = GetFilePathFromAppConfig(application.ConfigSectionName);
                application.ApplicationConfiguration = LoadAppConfig(false, filePath, application.ApplicationType, application.ConfigurationType, true).Result;

                bool certOK = application.CheckApplicationInstanceCertificate(false, 0).Result;
                if (!certOK)
                {
                    throw new Exception("Application instance certificate invalid!");
                }
                Application.Run(new Form1(application));

            }
            catch (Exception e)
            {
               
            }
        }
        public static async Task<ApplicationConfiguration> LoadAppConfig(
            bool silent,
            string filePath,
            ApplicationType applicationType,
            Type configurationType,
            bool applyTraceSettings)
        {
            Utils.Trace(Utils.TraceMasks.Information, "Loading application configuration file. {0}", filePath);

            try
            {
                // load the configuration file.
                ApplicationConfiguration configuration = await ApplicationConfiguration.Load(
                    new System.IO.FileInfo(filePath),
                    applicationType,
                    configurationType,
                    applyTraceSettings);

                if (configuration == null)
                {
                    return null;
                }

                return configuration;
            }
            catch (Exception e)
            {
                Utils.Trace(e, "Could not load configuration file. {0}", filePath);
                return null;
            }
        }
        public static string GetFilePathFromAppConfig(string sectionName)
        {
            // convert to absolute file path (expands environment strings).
            string absolutePath = $"{Directory.GetCurrentDirectory()}\\{sectionName}.Config.xml";
            if (absolutePath != null)
            {
                return absolutePath;
            }
            else
            {
                return sectionName + ".Config.xml";
            }
        }
    }
}
