﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Opc;
using Opc.Ua;
using Opc.Ua.Bindings.Custom;
using Opc.Ua.Client;
using Opc.Ua.Configuration;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public ApplicationInstance Application;
        public ServiceMessageContext Context;
        public ApplicationConfiguration Configuration;
        public Session _session;
        public Random rand = new Random();
        public double[] data = new double[30];
        public double[] data2 = new double[30];
        public double[] data3 = new double[30];
        public double[] data4 = new double[30];
        public int counter = 0;
        Opc.Ua.Server.StandardServer server;
        public Form1()
        {
            InitializeComponent();

        }
        public Form1(ApplicationInstance application)
        {
            Application = application;
            Context = application.ApplicationConfiguration.CreateMessageContext();
            Configuration = application.ApplicationConfiguration;

            InitializeComponent();

            if (!Configuration.SecurityConfiguration.AutoAcceptUntrustedCertificates)
            {
                Configuration.CertificateValidator.CertificateValidation += new CertificateValidationEventHandler(CertificateValidator_CertificateValidation);
            }
        }
        void CertificateValidator_CertificateValidation(CertificateValidator validator, CertificateValidationEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new CertificateValidationEventHandler(CertificateValidator_CertificateValidation), validator, e);
                return;
            }

            try
            {
                HandleCertificateValidationError(this, validator, e);
            }
            catch (Exception exception)
            {

            }
        }
        public static void HandleCertificateValidationError(Form caller, CertificateValidator validator, CertificateValidationEventArgs e)
        {
            StringBuilder buffer = new StringBuilder();

            buffer.AppendFormat("Certificate could not be validated!\r\n");
            buffer.AppendFormat("Validation error(s): \r\n");
            buffer.AppendFormat("\t{0}\r\n", e.Error.StatusCode);
            if (e.Error.InnerResult != null)
            {
                buffer.AppendFormat("\t{0}\r\n", e.Error.InnerResult.StatusCode);
            }
            buffer.AppendFormat("\r\nSubject: {0}\r\n", e.Certificate.Subject);
            buffer.AppendFormat("Issuer: {0}\r\n", (e.Certificate.Subject == e.Certificate.Issuer) ? "Self-signed" : e.Certificate.Issuer);
            buffer.AppendFormat("Valid From: {0}\r\n", e.Certificate.NotBefore);
            buffer.AppendFormat("Valid To: {0}\r\n", e.Certificate.NotAfter);
            buffer.AppendFormat("Thumbprint: {0}\r\n\r\n", e.Certificate.Thumbprint);
            buffer.AppendFormat("The security certificate was not issued by a trusted certificate authority. ");
            buffer.AppendFormat("Security certificate problems may indicate an attempt to intercept any data you send ");
            buffer.AppendFormat("to a server or to allow an untrusted client to connect to your server.");
            buffer.AppendFormat("\r\n\r\nAccept anyway?");

            if (MessageBox.Show(buffer.ToString(), caller.Text, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Accept = true;
            }
        }
        private void moveLeft()
        {
            for (int ind = 0; ind < 29; ind++)
            {
                data[ind] = data[ind + 1];
                data2[ind] = data2[ind + 1];

                data3[ind] = data3[ind + 1];
                data4[ind] = data4[ind + 1];
            }
            var q = rand.NextDouble() * (20 + 10) + 10;
            data[29] = Math.Sin(q);
            data2[29] = data[29] / q;
            data3[29] = Math.Sin(counter);
            data4[29] = data3[29] / counter;
            counter++;

            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            chart2.Series[0].Points.Clear();
            chart2.Series[1].Points.Clear();
            for (int i = 0; i < 30; i++)
            {
                chart1.Series[0].Points.AddXY(i, data[i]);
                chart1.Series[1].Points.AddXY(i, data2[i]);

                chart2.Series[0].Points.AddXY(i, data3[i]);
                chart2.Series[1].Points.AddXY(i, data4[i]);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //for (int i = 0; i < 30; i++)
            //    moveLeft();
            //    for (int i = 0; i < 30; i++)
            //        chart1.Series[0].Points.AddXY(i, data[i]);
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            moveLeft();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ITransportChannel channel = null;
            try
            {
                var cert = Configuration.SecurityConfiguration.ApplicationCertificate.Find(true).Result;
                X509Certificate2Collection clientCertificateChain = new X509Certificate2Collection(cert);

                List<CertificateIdentifier> issuers = new List<CertificateIdentifier>();
                var qq = Configuration.CertificateValidator.GetIssuers(cert, issuers).Result;
                for (int i = 0; i < issuers.Count; i++)
                {
                    clientCertificateChain.Add(issuers[i].Certificate);
                }

                ConfiguredEndpoint endpoint = new ConfiguredEndpoint();
                endpoint.EndpointUrl = new Uri(comboBox1.Text);
                endpoint.UpdateFromServer();
                EndpointConfiguration config = new EndpointConfiguration();
                config.OperationTimeout = Configuration.TransportQuotas.OperationTimeout;
                config.UseBinaryEncoding = true;
                config.MaxArrayLength = Configuration.TransportQuotas.MaxArrayLength;
                config.MaxByteStringLength = Configuration.TransportQuotas.MaxByteStringLength;
                config.MaxMessageSize = Configuration.TransportQuotas.MaxMessageSize;
                config.MaxStringLength = Configuration.TransportQuotas.MaxStringLength;
                config.MaxBufferSize = Configuration.TransportQuotas.MaxBufferSize;
                config.ChannelLifetime = Configuration.TransportQuotas.ChannelLifetime;
                config.SecurityTokenLifetime = Configuration.TransportQuotas.SecurityTokenLifetime;
                endpoint.Update(config);
                channel = SessionChannel.Create(
                    Configuration,
                    endpoint.Description,
                    endpoint.Configuration,
                    cert,
                    Configuration.SecurityConfiguration.SendCertificateChain ? clientCertificateChain : null,
                    Context);
                _session = new Session(channel, Configuration, endpoint, cert);
                _session.KeepAlive += new KeepAliveEventHandler(StandardClient_KeepAlive);
                StandardClient_KeepAlive(_session, null);
                _session.ReturnDiagnostics = DiagnosticsMasks.All;
                channel = null;
                var subscription = _session.DefaultSubscription;
                _session.AddSubscription(subscription);
                var policy = endpoint.SelectedUserTokenPolicy;
                if (policy == null)
                {
                    if (endpoint.Description.UserIdentityTokens.Count > 0)
                    {
                        policy = endpoint.Description.UserIdentityTokens[0];
                    }
                }
                if (policy != null)
                {
                   
                }
                    subscription.Create();
            }
            catch(Exception ex) { }
            finally
            {
                if (channel != null)
                    channel.Close();
            }
        }

        void StandardClient_KeepAlive(Session sender, KeepAliveEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new KeepAliveEventHandler(StandardClient_KeepAlive), sender, e);
                return;
            }
            else if (!IsHandleCreated)
            {
                return;
            }
        }

        //public async void Connect(ConfiguredEndpoint endpoint)
        //{
        //    if (endpoint == null)
        //    {
        //        return;
        //    }

        //    Session session = await SessionsCTRL.Connect(endpoint);

        //    if (session != null)
        //    {
        //        // stop any reconnect operation.
        //        if (m_reconnectHandler != null)
        //        {
        //            m_reconnectHandler.Dispose();
        //            m_reconnectHandler = null;
        //        }

        //        m_session = session;
        //        m_session.KeepAlive += new KeepAliveEventHandler(StandardClient_KeepAlive);
        //        BrowseCTRL.SetView(m_session, BrowseViewType.Objects, null);
        //        StandardClient_KeepAlive(m_session, null);
        //    }
        //}
    }
}
